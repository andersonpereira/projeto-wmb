import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:project_wmb/app/models/pokemon.dart';

part 'pokemon_new_store.g.dart';

class PokemonNewStore = _PokemonNewStoreBase with _$PokemonNewStore;

abstract class _PokemonNewStoreBase with Store {
  @action
  Future<bool> pushToFirestore(Pokemon pokemon) async {
    CollectionReference objRef =
        FirebaseFirestore.instance.collection('pokemon');

    objRef.add(pokemon.toJson()).then((res) {
      print('documentID:${res.id}');
    });
    return Future.value(true);
  }
}
